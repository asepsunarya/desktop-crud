package com.lynx.desktop.dao;

import com.lynx.desktop.entity.Jurusan;
import com.lynx.desktop.util.DaoService;
import com.lynx.desktop.util.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JurusanDaoImpl implements DaoService<Jurusan> {
    @Override
    public List<Jurusan> fetchAll() throws SQLException, ClassNotFoundException {
        List<Jurusan> jurusans = new ArrayList<>();
        String query = "SELECT id, nama FROM jurusan";
        try (Connection connection = MySQLConnection.createConnection()) {
            try(PreparedStatement ps = connection.prepareStatement(query)) {
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        Jurusan jurusan = new Jurusan();
                        jurusan.setId(rs.getInt("id"));
                        jurusan.setNama(rs.getString("nama"));
                        jurusans.add(jurusan);
                    }
                }
            }
        }

        return jurusans;
    }

    @Override
    public int addData(Jurusan jurusan) throws SQLException, ClassNotFoundException {
        int result = 0;
        String query = "INSERT INTO jurusan(nama) VALUES(?)";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setString(1, jurusan.getNama());
                if (ps.executeUpdate() != 0) {
                    connection.commit();
                    result = 1;
                } else {
                    connection.rollback();
                }
            }
        }
        return result;
    }

    @Override
    public int updateData(Jurusan jurusan) throws SQLException, ClassNotFoundException {
        int result = 0;
        String query = "UPDATE jurusan SET nama = ? WHERE id = ?";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setString(1, jurusan.getNama());
                ps.setInt(2, jurusan.getId());
                if (ps.executeUpdate() != 0) {
                    connection.commit();
                    result = 1;
                } else {
                    connection.rollback();
                }
            }
        }

        return result;
    }

    @Override
    public int deleteData(Jurusan jurusan) throws SQLException, ClassNotFoundException {
        int result = 0;
        String query = "DELETE FROM jurusan WHERE id = ?";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setInt(1, jurusan.getId());
                if (ps.executeUpdate() != 0) {
                    connection.commit();
                    result = 1;
                } else {
                    connection.rollback();
                }
            }
        }

        return result;
    }

}
