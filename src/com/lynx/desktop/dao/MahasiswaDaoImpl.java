package com.lynx.desktop.dao;

import com.lynx.desktop.entity.Jurusan;
import com.lynx.desktop.entity.Mahasiswa;
import com.lynx.desktop.util.DaoService;
import com.lynx.desktop.util.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MahasiswaDaoImpl implements DaoService<Mahasiswa> {

    @Override
    public List<Mahasiswa> fetchAll() throws SQLException, ClassNotFoundException {
        List<Mahasiswa> mahasiswas = new ArrayList<>();
        String query = "SELECT m.id, npm, m.nama, kelas, alamat, jurusan_id, j.nama as jurusan FROM " +
                "mahasiswa m JOIN jurusan j ON m.jurusan_id = j.id";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        Jurusan jurusan = new Jurusan();
                        jurusan.setId(rs.getInt("jurusan_id"));
                        jurusan.setNama(rs.getString("jurusan"));

                        Mahasiswa mahasiswa = new Mahasiswa();
                        mahasiswa.setId(rs.getInt("id"));
                        mahasiswa.setNpm(rs.getString("npm"));
                        mahasiswa.setNama(rs.getString("nama"));
                        mahasiswa.setKelas(rs.getString("kelas"));
                        mahasiswa.setAlamat(rs.getString("alamat"));
                        mahasiswa.setJurusan(jurusan);
                        mahasiswas.add(mahasiswa);
                    }
                }
            }
        }
        return mahasiswas;
    }

    @Override
    public int addData(Mahasiswa mahasiswa) throws SQLException, ClassNotFoundException {
        int result = 0;
        String query = "INSERT INTO mahasiswa(npm, nama, kelas, alamat, jurusan_id) VALUES(?, ?, ?, ?, ?)";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setString(1, mahasiswa.getNpm());
                ps.setString(2, mahasiswa.getNama());
                ps.setString(3, mahasiswa.getKelas());
                ps.setString(4, mahasiswa.getAlamat());
                ps.setInt(5, mahasiswa.getJurusan().getId());
                if (ps.executeUpdate() != 0) {
                    connection.commit();
                    result = 1;
                } else {
                    connection.rollback();
                }
            }
        }
        return result;
    }

    @Override
    public int updateData(Mahasiswa mahasiswa) throws SQLException, ClassNotFoundException {
        int result = 0;
        String query = "UPDATE mahasiswa SET npm = ?, nama = ?, kelas = ?, alamat = ?, jurusan_id = ? WHERE id = ?";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setString(1, mahasiswa.getNpm());
                ps.setString(2, mahasiswa.getNama());
                ps.setString(3, mahasiswa.getKelas());
                ps.setString(4, mahasiswa.getAlamat());
                ps.setInt(5, mahasiswa.getJurusan().getId());

                ps.setInt(6, mahasiswa.getId());
                if (ps.executeUpdate() != 0) {
                    connection.commit();
                    result = 1;
                } else {
                    connection.rollback();
                }
            }
        }

        return result;
    }

    @Override
    public int deleteData(Mahasiswa mahasiswa) throws SQLException, ClassNotFoundException {
        int result = 0;
        String query = "DELETE FROM mahasiswa WHERE id = ?";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setInt(1, mahasiswa.getId());
                if (ps.executeUpdate() != 0) {
                    connection.commit();
                    result = 1;
                } else {
                    connection.rollback();
                }
            }
        }
        return result;
    }

}
