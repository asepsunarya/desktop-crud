package com.lynx.desktop.dao;

import com.lynx.desktop.entity.Role;
import com.lynx.desktop.util.DaoService;
import com.lynx.desktop.util.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class RoleDaoImpl implements DaoService<Role> {
    @Override
    public List<Role> fetchAll() throws SQLException, ClassNotFoundException {
        List<Role> roles = new ArrayList<>();
        String query = "SELECT id, nama FROM role";
        try (Connection connection = MySQLConnection.createConnection()) {
            try(PreparedStatement ps = connection.prepareStatement(query)) {
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        Role role = new Role();
                        role.setId(rs.getInt("id"));
                        role.setNama(rs.getString("nama"));
                        roles.add(role);
                    }
                }
            }
        }

        return roles;
    }

    @Override
    public int addData(Role role) throws SQLException, ClassNotFoundException {
        int result = 0;
        String query = "INSERT INTO role(nama) VALUES(?)";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setString(1, role.getNama());
                if (ps.executeUpdate() != 0) {
                    connection.commit();
                    result = 1;
                } else {
                    connection.rollback();
                }
            }
        }
        return result;
    }

    @Override
    public int updateData(Role role) throws SQLException, ClassNotFoundException {
        int result = 0;
        String query = "UPDATE role SET nama = ? WHERE id = ?";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setString(1, role.getNama());
                ps.setInt(2, role.getId());
                if (ps.executeUpdate() != 0) {
                    connection.commit();
                    result = 1;
                } else {
                    connection.rollback();
                }
            }
        }

        return result;
    }

    @Override
    public int deleteData(Role role) throws SQLException, ClassNotFoundException {
        int result = 0;
        String query = "DELETE FROM role WHERE id = ?";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setInt(1, role.getId());
                if (ps.executeUpdate() != 0) {
                    connection.commit();
                    result = 1;
                } else {
                    connection.rollback();
                }
            }
        }

        return result;
    }
}
