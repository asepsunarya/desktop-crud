package com.lynx.desktop.dao;

import com.lynx.desktop.entity.Jurusan;
import com.lynx.desktop.entity.Mahasiswa;
import com.lynx.desktop.entity.Role;
import com.lynx.desktop.entity.User;
import com.lynx.desktop.util.DaoService;
import com.lynx.desktop.util.MySQLConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements DaoService<User> {
    @Override
    public List<User> fetchAll() throws SQLException, ClassNotFoundException {
        List<User> users = new ArrayList<>();
        String query = "SELECT " +
                "u.id, username, password, role_id, mahasiswa_id, u.created_at, u.updated_at," +
                "r.nama as role," +
                "npm, m.nama, kelas, alamat, jurusan_id, " +
                "j.nama as jurusan FROM " +
                "users u JOIN role r ON u.role_id = r.id " +
                "JOIN mahasiswa m ON u.mahasiswa_id = m.id " +
                "JOIN jurusan j ON m.jurusan_id = j.id";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                try (ResultSet rs = ps.executeQuery()) {
                    while (rs.next()) {
                        Role role = new Role();
                        role.setId(rs.getInt("role_id"));
                        role.setNama(rs.getString("role"));

                        Jurusan jurusan = new Jurusan();
                        jurusan.setId(rs.getInt("jurusan_id"));
                        jurusan.setNama(rs.getString("jurusan"));

                        Mahasiswa mahasiswa = new Mahasiswa();
                        mahasiswa.setId(rs.getInt("mahasiswa_id"));
                        mahasiswa.setNpm(rs.getString("npm"));
                        mahasiswa.setNama(rs.getString("nama"));
                        mahasiswa.setKelas(rs.getString("kelas"));
                        mahasiswa.setAlamat(rs.getString("alamat"));
                        mahasiswa.setJurusan(jurusan);

                        User user = new User();
                        user.setId(rs.getInt("id"));
                        user.setUsername(rs.getString("username"));
                        user.setPassword(rs.getString("password"));
                        user.setRole(role);
                        user.setMahasiswa(mahasiswa);
                        user.setCreated_at(rs.getString("created_at"));
                        user.setUpdated_at(rs.getString("updated_at"));
                        users.add(user);
                    }
                }
            }
        }
        return users;
    }

    @Override
    public int addData(User user) throws SQLException, ClassNotFoundException {
        int result = 0;
        String query = "INSERT INTO users(username, password, role_id, mahasiswa_id) VALUES(?, ?, ?, ?)";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setString(1, user.getUsername());
                ps.setString(2, user.getPassword());
                ps.setInt(3, user.getRole().getId());
                ps.setInt(4, user.getMahasiswa().getId());
                if (ps.executeUpdate() != 0) {
                    connection.commit();
                    result = 1;
                } else {
                    connection.rollback();
                }
            }
        }
        return result;
    }

    @Override
    public int updateData(User user) throws SQLException, ClassNotFoundException {
        int result = 0;
        String query = "UPDATE users SET username = ?, password = ?, role_id = ?, mahasiswa_id = ? WHERE id = ?";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setString(1, user.getUsername());
                ps.setString(2, user.getPassword());
                ps.setInt(3, user.getRole().getId());
                ps.setInt(4, user.getMahasiswa().getId());

                ps.setInt(5, user.getId());
                if (ps.executeUpdate() != 0) {
                    connection.commit();
                    result = 1;
                } else {
                    connection.rollback();
                }
            }
        }

        return result;
    }

    @Override
    public int deleteData(User user) throws SQLException, ClassNotFoundException {
        int result = 0;
        String query = "DELETE FROM users WHERE id = ?";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setInt(1, user.getId());
                if (ps.executeUpdate() != 0) {
                    connection.commit();
                    result = 1;
                } else {
                    connection.rollback();
                }
            }
        }

        return result;
    }


    public boolean login(String username, String password) throws SQLException, ClassNotFoundException {
        String query = "SELECT id, username, password FROM users WHERE username = ? AND password = ?";
        try (Connection connection = MySQLConnection.createConnection()) {
            try (PreparedStatement ps = connection.prepareStatement(query)) {
                ps.setString(1, username);
                ps.setString(2, password);
                try (ResultSet rs = ps.executeQuery()) {
                    if (rs.next()) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        }
    }
}
