package com.lynx.desktop.entity;

public class Jurusan {
    private int id;
    private String nama;

    public Jurusan() {
    }

    public Jurusan(int id, String nama) {
        this.id = id;
        this.nama = nama;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
//    penting ieu teh meh teu com.lynx.desktop.entity.Jurusan@aujdas27
    @Override
    public String toString() {
        return nama;
    }
}
