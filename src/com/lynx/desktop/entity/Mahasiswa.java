package com.lynx.desktop.entity;

public class Mahasiswa {
    private int id;
    private String npm;
    private String nama;
    private String kelas;
    private String alamat;
    private Jurusan jurusan;

    public Mahasiswa() {
    }

    public Mahasiswa(int id, String npm, String nama, String kelas, String alamat, Jurusan jurusan) {
        this.id = id;
        this.npm = npm;
        this.nama = nama;
        this.kelas = kelas;
        this.alamat = alamat;
        this.jurusan = jurusan;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNpm() {
        return npm;
    }

    public void setNpm(String npm) {
        this.npm = npm;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKelas() {
        return kelas;
    }

    public void setKelas(String kelas) {
        this.kelas = kelas;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public Jurusan getJurusan() {
        return jurusan;
    }

    public void setJurusan(Jurusan jurusan) {
        this.jurusan = jurusan;
    }
    //    penting ieu teh meh teu com.lynx.desktop.entity.Role@aujdas27
    @Override
    public String toString() {
        return nama;
    }
}
