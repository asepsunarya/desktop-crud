package com.lynx.desktop.entity;

public class User {
    private int id;
    private String username;
    private String password;
    private Role role;
    private Mahasiswa mahasiswa;
    private String created_at;
    private String updated_at;

    public User() {
    }

    public User(int id, String username, String password, Role role, Mahasiswa mahasiswa, String created_at, String updated_at) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.role = role;
        this.mahasiswa = mahasiswa;
        this.created_at = created_at;
        this.updated_at = updated_at;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Mahasiswa getMahasiswa() {
        return mahasiswa;
    }

    public void setMahasiswa(Mahasiswa mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}
