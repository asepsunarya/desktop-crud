package com.lynx.desktop.view;

import com.lynx.desktop.dao.UserDaoImpl;

import javax.swing.*;
import java.sql.SQLException;

public class LoginForm extends JFrame {
    private JTextField txtUsername;
    private JPanel rootPanel;
    private JTextField txtPassword;
    private JButton btnLogin;
    private JFrame frame;

    private UserDaoImpl userDao;

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        JFrame frame = new JFrame("Login Form");
        frame.setContentPane(new LoginForm().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(250, 250);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public LoginForm() throws SQLException, ClassNotFoundException{
        userDao = new UserDaoImpl();

        btnLogin.addActionListener(e -> {
            if (txtUsername.getText().trim().isEmpty() ||
                    txtPassword.getText().trim().isEmpty()) {
                JOptionPane.showMessageDialog(rootPanel, "Silahkan isi Username dan Password", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                try {
                    if (userDao.login(txtUsername.getText(), txtPassword.getText()) == true) {
                        JFrame frame = new JFrame("User Form");
                        frame.setContentPane(new UserForm().rootPanel);
                        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                        frame.pack();
                        frame.setSize(800, 600);
                        frame.setLocationRelativeTo(null);
                        frame.setVisible(true);
                    } else {
                        JOptionPane.showMessageDialog(rootPanel, "Username atau Password yang anda masukan salah!", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                } catch (ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });
    }
}
