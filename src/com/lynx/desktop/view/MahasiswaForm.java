package com.lynx.desktop.view;

import com.lynx.desktop.dao.JurusanDaoImpl;
import com.lynx.desktop.dao.MahasiswaDaoImpl;
import com.lynx.desktop.entity.Jurusan;
import com.lynx.desktop.entity.Mahasiswa;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MahasiswaForm {
    private JTextField txtNpm;
    private JTextField txtNama;
    private JTextField txtKelas;
    private JTextArea txtAlamat;
    private JComboBox<Jurusan> comboJurusan;
    private JButton btnTambahJurusan;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JButton btnSave;
    private JTable tableMahasiswa;
    JSplitPane rootPanel;
    private JButton btnReset;

    private JurusanDaoImpl jurusanDao;
    private MahasiswaDaoImpl mahasiswaDao;

    private List<Jurusan> jurusans;
    private List<Mahasiswa> mahasiswas;

    private DefaultComboBoxModel<Jurusan> jurusanComboBoxModel;
    private MahasiswaTableModel mahasiswaTableModel;

    private Mahasiswa selectedMahasiswa;

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        JFrame frame = new JFrame("Mahasiswa Form");
        frame.setContentPane(new MahasiswaForm().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public MahasiswaForm() throws SQLException, ClassNotFoundException {
        jurusanDao = new JurusanDaoImpl();
        mahasiswaDao = new MahasiswaDaoImpl();
        jurusans = new ArrayList<>();
        mahasiswas = new ArrayList<>();
        try {
            jurusans.addAll(jurusanDao.fetchAll());
            mahasiswas.addAll(mahasiswaDao.fetchAll());
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        jurusanComboBoxModel = new DefaultComboBoxModel<>(jurusans.toArray(new Jurusan[0]));
        comboJurusan.setModel(jurusanComboBoxModel);
        mahasiswaTableModel = new MahasiswaTableModel(mahasiswas);
        tableMahasiswa.setModel(mahasiswaTableModel);
        tableMahasiswa.setAutoCreateRowSorter(true);
        listener();
    }

    private void listener() {
        btnTambahJurusan.addActionListener(e -> {
            String newJurusan = JOptionPane.showInputDialog(rootPanel, "Nama Jurusan Baru");
            if (newJurusan != null && !newJurusan.trim().isEmpty()) {
                Jurusan jurusan = new Jurusan();
                jurusan.setNama(newJurusan);
                try {
                    if(jurusanDao.addData(jurusan) == 1) {
                        jurusans.clear();
                        jurusans.addAll(jurusanDao.fetchAll());
                        jurusanComboBoxModel.removeAllElements();
                        jurusanComboBoxModel.addAll(jurusans);
                    }
                } catch (SQLException | ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });
        btnSave.addActionListener(e -> {
            if (txtNpm.getText().trim().isEmpty() ||
                    txtNama.getText().trim().isEmpty() ||
                    txtKelas.getText().trim().isEmpty() ||
                    txtAlamat.getText().trim().isEmpty() ||
                    comboJurusan.getSelectedItem() == null
            ) {
                JOptionPane.showMessageDialog(rootPanel, "Silahkan isi NPM, Nama, Kelas, Alamat, Jurusan", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                Mahasiswa mahasiswa = new Mahasiswa();
                mahasiswa.setNpm(txtNpm.getText());
                mahasiswa.setNama(txtNama.getText());
                mahasiswa.setKelas(txtKelas.getText());
                mahasiswa.setAlamat(txtAlamat.getText());
                mahasiswa.setJurusan((Jurusan) comboJurusan.getSelectedItem());
                try {
                    if (mahasiswaDao.addData(mahasiswa) == 1) {
                        mahasiswas.clear();
                        mahasiswas.addAll(mahasiswaDao.fetchAll());
                        mahasiswaTableModel.fireTableDataChanged();
                        clearAndReset();
                    }
                } catch (SQLException | ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });
        btnReset.addActionListener(e -> {
            clearAndReset();
        });
        btnDelete.addActionListener(e -> {
            try {
                if (mahasiswaDao.deleteData(selectedMahasiswa) == 1) {
                    mahasiswaTableModel.fireTableDataChanged();
                    clearAndReset();
                }
            } catch (SQLException | ClassNotFoundException ex) {
                ex.printStackTrace();
            }
        });
        btnUpdate.addActionListener(e -> {
            if (txtNpm.getText().trim().isEmpty() ||
                    txtNama.getText().trim().isEmpty() ||
                    txtKelas.getText().trim().isEmpty() ||
                    txtAlamat.getText().trim().isEmpty() ||
                    comboJurusan.getSelectedItem() == null
            ) {
                JOptionPane.showMessageDialog(rootPanel, "Silahkan isi NPM, Nama, Kelas, Alamat, Jurusan", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                selectedMahasiswa.setNama(txtNama.getText());
                selectedMahasiswa.setKelas(txtKelas.getText());
                selectedMahasiswa.setAlamat(txtAlamat.getText());
                selectedMahasiswa.setJurusan((Jurusan) comboJurusan.getSelectedItem());
                try {
                    if (mahasiswaDao.updateData(selectedMahasiswa) == 1) {
                        mahasiswas.clear();
                        mahasiswas.addAll(mahasiswaDao.fetchAll());
                        mahasiswaTableModel.fireTableDataChanged();
                        clearAndReset();
                    }
                } catch (SQLException | ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });

        tableMahasiswa.getSelectionModel().addListSelectionListener(e -> {
            if(!tableMahasiswa.getSelectionModel().isSelectionEmpty()) {
                int selectedIndex = tableMahasiswa.convertRowIndexToModel(tableMahasiswa.getSelectedRow());
                selectedMahasiswa = mahasiswas.get(selectedIndex);
                if (selectedMahasiswa != null) {
                    txtNpm.setText(selectedMahasiswa.getNpm());
                    txtNama.setText(selectedMahasiswa.getNama());
                    txtKelas.setText(selectedMahasiswa.getKelas());
                    txtAlamat.setText(selectedMahasiswa.getAlamat());
                    comboJurusan.setSelectedItem(selectedMahasiswa.getJurusan());
                    txtNpm.setEnabled(false);
                    btnSave.setEnabled(false);
                    btnUpdate.setEnabled(true);
                }
            }
        });
    }

    private void clearAndReset() {
        txtNpm.setText("");
        txtNama.setText("");
        txtKelas.setText("");
        txtAlamat.setText("");
        txtNpm.setEnabled(true);
        btnSave.setEnabled(true);
        btnUpdate.setEnabled(false);
        tableMahasiswa.clearSelection();
        selectedMahasiswa = null;
    }

    private static class MahasiswaTableModel extends AbstractTableModel {

        private List<Mahasiswa> mahasiswas;
        private final String[] COLUMNS = {"ID", "NPM", "NAMA", "KELAS", "ALAMAT", "JURUSAN"};

        public MahasiswaTableModel(List<Mahasiswa> mahasiswas) {
            this.mahasiswas = mahasiswas;
        }

        @Override
        public int getRowCount() {
            return mahasiswas.size();
        }

        @Override
        public int getColumnCount() {
            return COLUMNS.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            return switch (columnIndex) {
                case 0 -> mahasiswas.get(rowIndex).getId();
                case 1 -> mahasiswas.get(rowIndex).getNpm();
                case 2 -> mahasiswas.get(rowIndex).getNama();
                case 3 -> mahasiswas.get(rowIndex).getKelas();
                case 4 -> mahasiswas.get(rowIndex).getAlamat();
                case 5 -> mahasiswas.get(rowIndex).getJurusan().getNama();
                default -> "";
            };
        }

        @Override
        public String getColumnName(int column) {
            return COLUMNS[column];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            if (getValueAt(0, columnIndex) != null) {
                return getValueAt(0, columnIndex).getClass();
            }
            return Object.class;
        }
    }
}
