package com.lynx.desktop.view;

import com.lynx.desktop.dao.MahasiswaDaoImpl;
import com.lynx.desktop.dao.RoleDaoImpl;
import com.lynx.desktop.dao.UserDaoImpl;
import com.lynx.desktop.entity.Mahasiswa;
import com.lynx.desktop.entity.Role;
import com.lynx.desktop.entity.User;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserForm {
    private JTextField txtUsername;
    private JTextField txtPassword;
    private JComboBox comboRole;
    private JButton btnTambahRole;
    private JComboBox comboMahasiswa;
    private JButton btnTambahMahasiswa;
    private JButton btnUpdate;
    private JButton btnDelete;
    private JButton btnSave;
    private JTable tableUser;
    JSplitPane rootPanel;
    private JButton btnReset;

    private UserDaoImpl userDao;
    private RoleDaoImpl roleDao;
    private MahasiswaDaoImpl mahasiswaDao;

    private List<User> users;
    private List<Role> roles;
    private List<Mahasiswa> mahasiswas;

    private DefaultComboBoxModel<Role> roleComboBoxModel;
    private DefaultComboBoxModel<Mahasiswa> mahasiswaComboBoxModel;
    private UserTableModel userTableModel;

    private User selectedUser;

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        JFrame frame = new JFrame("User Form");
        frame.setContentPane(new UserForm().rootPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public UserForm() throws SQLException, ClassNotFoundException {
        userDao = new UserDaoImpl();
        roleDao = new RoleDaoImpl();
        mahasiswaDao = new MahasiswaDaoImpl();

        users = new ArrayList<>();
        roles = new ArrayList<>();
        mahasiswas = new ArrayList<>();
        try {
            users.addAll(userDao.fetchAll());
            roles.addAll(roleDao.fetchAll());
            mahasiswas.addAll(mahasiswaDao.fetchAll());
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        roleComboBoxModel = new DefaultComboBoxModel<>(roles.toArray(new Role[0]));
        mahasiswaComboBoxModel = new DefaultComboBoxModel<>(mahasiswas.toArray(new Mahasiswa[0]));

        comboRole.setModel(roleComboBoxModel);
        comboMahasiswa.setModel(mahasiswaComboBoxModel);

        userTableModel = new UserTableModel(users);
        tableUser.setModel(userTableModel);
        tableUser.setAutoCreateRowSorter(true);

        listener();
    }

    private void listener() {
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);

        btnTambahRole.addActionListener(e -> {
            String newRole = JOptionPane.showInputDialog(rootPanel, "Nama Role Baru");
            if (newRole != null && !newRole.trim().isEmpty()) {
                Role role = new Role();
                role.setNama(newRole);
                try {
                    if(roleDao.addData(role) == 1) {
                        roles.clear();
                        roles.addAll(roleDao.fetchAll());
                        roleComboBoxModel.removeAllElements();
                        roleComboBoxModel.addAll(roles);
                    }
                } catch (SQLException | ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });
        btnTambahMahasiswa.addActionListener(e -> {
            JFrame frame = new JFrame("MainForm");
            try {
                frame.setContentPane(new MahasiswaForm().rootPanel);
            } catch (SQLException ex) {
                ex.printStackTrace();
            } catch (ClassNotFoundException ex) {
                ex.printStackTrace();
            }
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.pack();
            frame.setSize(800, 600);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        });
        btnSave.addActionListener(e -> {
            if (txtUsername.getText().trim().isEmpty() ||
                    txtPassword.getText().trim().isEmpty() ||
                    comboRole.getSelectedItem() == null ||
                    comboMahasiswa.getSelectedItem() == null
            ) {
                JOptionPane.showMessageDialog(rootPanel, "Silahkan isi Username, Password, Role, Mahasiswa", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                User user = new User();
                user.setUsername(txtUsername.getText());
                user.setPassword(txtPassword.getText());
                user.setRole((Role) comboRole.getSelectedItem());
                user.setMahasiswa((Mahasiswa) comboMahasiswa.getSelectedItem());
                try {
                    if (userDao.addData(user) == 1) {
                        users.clear();
                        users.addAll(userDao.fetchAll());
                        userTableModel.fireTableDataChanged();
                        clearAndReset();
                    }
                } catch (SQLException | ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });
        btnReset.addActionListener(e -> {
            clearAndReset();
        });
        btnDelete.addActionListener(e -> {
            int result = JOptionPane.showConfirmDialog(rootPanel, "Hapus User?");
            if(result == JOptionPane.YES_OPTION) {
                try {
                    if (userDao.deleteData(selectedUser) == 1) {
                        users.clear();
                        users.addAll(userDao.fetchAll());
                        userTableModel.fireTableDataChanged();
                        clearAndReset();
                    }
                } catch (SQLException | ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });
        btnUpdate.addActionListener(e -> {
            if (txtUsername.getText().trim().isEmpty() ||
                    txtPassword.getText().trim().isEmpty() ||
                    comboRole.getSelectedItem() == null ||
                    comboMahasiswa.getSelectedItem() == null
            ) {
                JOptionPane.showMessageDialog(rootPanel, "Silahkan isi Username, Password, Role, Mahasiswa", "Error", JOptionPane.ERROR_MESSAGE);
            } else {
                selectedUser.setUsername(txtUsername.getText());
                selectedUser.setPassword(txtPassword.getText());
                selectedUser.setRole((Role) comboRole.getSelectedItem());
                selectedUser.setMahasiswa((Mahasiswa) comboMahasiswa.getSelectedItem());
                try {
                    if (userDao.updateData(selectedUser) == 1) {
                        users.clear();
                        users.addAll(userDao.fetchAll());
                        userTableModel.fireTableDataChanged();
                        clearAndReset();
                    }
                } catch (SQLException | ClassNotFoundException ex) {
                    ex.printStackTrace();
                }
            }
        });

        tableUser.getSelectionModel().addListSelectionListener(e -> {
            if(!tableUser.getSelectionModel().isSelectionEmpty()) {
                int selectedIndex = tableUser.convertRowIndexToModel(tableUser.getSelectedRow());
                selectedUser = users.get(selectedIndex);
                if (selectedUser != null) {
                    txtUsername.setText(selectedUser.getUsername());
                    txtPassword.setText(selectedUser.getPassword());
                    comboRole.setSelectedItem(selectedUser.getRole());
                    txtUsername.setEnabled(false);
                    btnSave.setEnabled(false);
                    btnDelete.setEnabled(true);
                    btnUpdate.setEnabled(true);
                }
            }
        });
    }

    private void clearAndReset() {
        txtUsername.setText("");
        txtPassword.setText("");
        txtUsername.setEnabled(true);
        btnSave.setEnabled(true);
        btnUpdate.setEnabled(false);
        btnDelete.setEnabled(false);
        tableUser.clearSelection();
        selectedUser = null;
    }

    public static class UserTableModel extends AbstractTableModel {
        private List<User> users;
        private final String[] COLUMNS = {"ID", "NAMA", "USERNAME", "PASSWORD", "ROLE", "CREATED AT", "UPDATED AT"};

        public UserTableModel(List<User> users) { this.users = users; }

        @Override
        public int getRowCount() { return users.size(); }

        @Override
        public int getColumnCount() {
            return COLUMNS.length;
        }

        @Override
        public Object getValueAt(int rowIndex, int columnIndex) {
            return switch (columnIndex) {
                case 0 -> users.get(rowIndex).getId();
                case 1 -> users.get(rowIndex).getMahasiswa().getNama();
                case 2 -> users.get(rowIndex).getUsername();
                case 3 -> users.get(rowIndex).getPassword();
                case 4 -> users.get(rowIndex).getRole().getNama();
                case 5 -> users.get(rowIndex).getCreated_at();
                case 6 -> users.get(rowIndex).getUpdated_at();
                default -> "";
            };
        }

        @Override
        public String getColumnName(int column) {
            return COLUMNS[column];
        }

        @Override
        public Class<?> getColumnClass(int columnIndex) {
            if (getValueAt(0, columnIndex) != null) {
                return getValueAt(0, columnIndex).getClass();
            }
            return Object.class;
        }
    }
}
